#!/bin/sh

#Oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
#plugins
#syntax-highlighting
zsh -c "$(git clone --depth=1 https://github.com/zsh-users/zsh-syntax-highlighting.git \
	  ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting)"
#autosuggestions
zsh -c "$(git clone --depth=1 https://github.com/zsh-users/zsh-autosuggestions \
	  ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions)"
#aliases
zsh -c "$(git clone --depth=1 https://gitlab.com/crestfalln/zsh-aliases.git \
	  ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/alias)"
#powerlevel10k
zsh -c "$(git clone --depth=1 https://github.com/romkatv/powerlevel10k.git \
	  ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k)"

#config
git clone --depth=1 https://gitlab.com/crestfalln/dotfiles.git $HOME/dotfiles
ln -sf $HOME/dotfiles/.p10k.zsh $HOME/.p10k.zsh
ln -sf $HOME/dotfiles/nvim/init.vim $HOME/.config/nvim/init.vim
ln -sf $HOME/dotfiles/.zshrc $HOME/.zshrc

#nvim
#vim-plug
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
