--[[
lvim is the global options object

Linters should be
filled in as strings with either
a global executable or a path to
an executable
]]
-- THESE ARE EXAMPLE CONFIGS FEEL FREE TO CHANGE TO WHATEVER YOU WANT

local function changefont(step)
  local current_size = vim.o.guifont:match(':h(%d+)$')
  local current_font = vim.o.guifont:match('^(.+):h%d+$')
  local new_size = current_size + step
  local guifont_new = current_font .. ':' .. ('h' .. new_size)
  vim.o.guifont = guifont_new
end

function ZoomOut()
  return changefont(-1)
end

function ZoomIn()
  return changefont(1)
end

function FullScreenToggle()
  local new_value = not vim.g.neovide_fullscreen
  if(new_value == true)
  then
    new_value = 'v:true'
  else
    new_value = 'v:false'
  end
  vim.cmd('let g:neovide_fullscreen=' .. new_value)
end


-- general
lvim.log.level = "warn"
lvim.format_on_save = false
lvim.lsp.diagnostics.virtual_text = false --lvim.colorscheme = "on"
lvim.lsp.buffer_mappings.normal_mode["gh"] = {"<cmd>lua vim.lsp.buf.hover()<CR>", "Hover"}

-- keymappings [view all the defaults by pressing <leader>Lk]
lvim.leader = "space"
vim.o.guifont = 'MesloLGS NF:h13'
vim.o.timeoutlen = 200
-- add your own keymapping
-- lvim.keys.normal_mode["<C-s>"] = ":w<cr>"
lvim.keys.normal_mode["<jj>"] = "<esc>"
lvim.keys.normal_mode["<C-I>"] = ":lua vim.lsp.buf.formatting()<CR>"
lvim.keys.normal_mode["<C-+>"] = ":lua ZoomIn()<CR>"
lvim.keys.normal_mode["<C-->"] = ":lua ZoomOut()<CR>"
lvim.keys.insert_mode["<C-+>"] = ":lua ZoomIn()<CR>"
lvim.keys.insert_mode["<C-->"] = ":lua ZoomOut()<CR>"
lvim.keys.normal_mode["<F11>"] = ":lua FullScreenToggle()<CR>"
lvim.keys.insert_mode["<C-V>"] = "<C-r>+"
-- unmap a default keymapping
-- lvim.keys.normal_mode["<C-Up>"] = false
-- edit a default keymapping
-- lvim.keys.normal_mode["<C-q>"] = ":q<cr>"

-- Change Telescope navigation to use j and k for navigation and n and p for history in both input and normal mode.
-- we use protected-mode (pcall) just in case the plugin wasn't loaded yet.
-- local _, actions = pcall(require, "telescope.actions")
-- lvim.builtin.telescope.defaults.mappings = {
--   -- for input mode
--   i = {
--     ["<C-j>"] = actions.move_selection_next,
--     ["<C-k>"] = actions.move_selection_previous,
--     ["<C-n>"] = actions.cycle_history_next,
--     ["<C-p>"] = actions.cycle_history_prev,
--   },
--   -- for normal mode
--   n = {
--     ["<C-j>"] = actions.move_selection_next,
--     ["<C-k>"] = actions.move_selection_previous,
--   },
-- }
lvim.builtin.which_key.mappings["V"] = {"<cmd>split<cr>", "Split Horizontally"}
lvim.builtin.which_key.mappings["v"] = {"<cmd>vsplit<cr>", "Split Vertically"}
lvim.builtin.which_key.mappings["t"] = {"<cmd>ToggleTerm size=15 direction=horizontal<cr>",
                                        "Toggle Terminal"}
-- local which_key = lvim.builtin.which_key
-- which_key.mappings = {
--   -- ["a"] = { "<cmd>Alpha<cr>", "Alpha" },
--   ["b"] = {
--     "<cmd>lua require('telescope.builtin').buffers(require('telescope.themes').get_dropdown{previewer = false})<cr>",
--     "Buffers",
--   },
--   ["e"] = { "<cmd>NvimTreeToggle<cr>", "Explorer" },
--   ["w"] = { "<cmd>w!<CR>", "Save" },
--   ["q"] = { "<cmd>q!<CR>", "Quit" },
--   ["x"] = { "<cmd>x<CR>", "Save and Quit" },
--   ["c"] = { "<cmd>w!<CR><cmd>bd<CR>", "Close Buffer" },
--   ["h"] = { "<cmd>nohlsearch<CR>", "No Highlight" },
--   ["f"] = {
--     "<cmd>lua require('telescope.builtin').find_files(require('telescope.themes').get_dropdown{previewer = false})<cr>",
--     -- "<cmd>lua require('telescope.builtin').find_files(require('telescope.themes').get_dropdown({previewer = true})<CR>",
--     "Find files",
--   },
--   ["F"] = { "<cmd>Telescope live_grep theme=ivy<cr>", "Find Text" },
--   ["P"] = { "<cmd>Telescope projects<cr>", "Projects" },
--   ["v"] = { "<cmd>vsplit<cr>", "Split Vertically"},
--   ["V"] = { "<cmd>split<cr>", "Split Horizontally"},
--   ["t"] = {
--     "<cmd>ToggleTerm size=10 direction=horizontal<CR>",
--     "Toggle Terminal",
--   },
--   p = {
--     name = "Packer",
--     c = { "<cmd>PackerCompile<cr>", "Compile" },
--     i = { "<cmd>PackerInstall<cr>", "Install" },
--     s = { "<cmd>PackerSync<cr>", "Sync" },
--     S = { "<cmd>PackerStatus<cr>", "Status" },
--     u = { "<cmd>PackerUpdate<cr>", "Update" },
--   },

--   g = {
--     name = "Git",
--     g = { "<cmd>lua _LAZYGIT_TOGGLE()<CR>", "Lazygit" },
--     j = { "<cmd>lua require 'gitsigns'.next_hunk()<cr>", "Next Hunk" },
--     k = { "<cmd>lua require 'gitsigns'.prev_hunk()<cr>", "Prev Hunk" },
--     l = { "<cmd>lua require 'gitsigns'.blame_line()<cr>", "Blame" },
--     p = { "<cmd>lua require 'gitsigns'.preview_hunk()<cr>", "Preview Hunk" },
--     r = { "<cmd>lua require 'gitsigns'.reset_hunk()<cr>", "Reset Hunk" },
--     R = { "<cmd>lua require 'gitsigns'.reset_buffer()<cr>", "Reset Buffer" },
--     s = { "<cmd>lua require 'gitsigns'.stage_hunk()<cr>", "Stage Hunk" },
--     u = {
--       "<cmd>lua require 'gitsigns'.undo_stage_hunk()<cr>",
--       "Undo Stage Hunk",
--     },
--     o = { "<cmd>Telescope git_status<cr>", "Open changed file" },
--     b = { "<cmd>Telescope git_branches<cr>", "Checkout branch" },
--     c = { "<cmd>Telescope git_commits<cr>", "Checkout commit" },
--     d = {
--       "<cmd>Gitsigns diffthis HEAD<cr>",
--       "Diff",
--     },
--   },

--   l = {
--     name = "LSP",
--     a = { "<cmd>lua vim.lsp.buf.code_action()<cr>", "Code Action" },
--     d = {
--       "<cmd>Telescope lsp_document_diagnostics<cr>",
--       "Document Diagnostics", }, w = {
--       "<cmd>Telescope lsp_workspace_diagnostics<cr>",
--       "Workspace Diagnostics",
--     },
--     f = { "<cmd>lua vim.lsp.buf.formatting()<cr>", "Format" },
--     i = { "<cmd>LspInfo<cr>", "Info" },
--     I = { "<cmd>LspInstallInfo<cr>", "Installer Info" },
--     j = {
--       "<cmd>lua vim.lsp.diagnostic.goto_next()<CR>",
--       "Next Diagnostic",
--     },
--     k = {
--       "<cmd>lua vim.lsp.diagnostic.goto_prev()<cr>",
--       "Prev Diagnostic",
--     },
--     l = { "<cmd>lua vim.lsp.codelens.run()<cr>", "CodeLens Action" },
--     q = { "<cmd>lua vim.lsp.diagnostic.set_loclist()<cr>", "Quickfix" },
--     r = { "<cmd>lua vim.lsp.buf.rename()<cr>", "Rename" },
--     s = { "<cmd>Telescope lsp_document_symbols<cr>", "Document Symbols" },
--     S = {
--       "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>",
--       "Workspace Symbols",
--     },
--   },
--   s = {
--     name = "Search",
--     b = { "<cmd>Telescope git_branches<cr>", "Checkout branch" },
--     c = { "<cmd>Telescope colorscheme<cr>", "Colorscheme" },
--     h = { "<cmd>Telescope help_tags<cr>", "Find Help" },
--     M = { "<cmd>Telescope man_pages<cr>", "Man Pages" },
--     r = { "<cmd>Telescope oldfiles<cr>", "Open Recent File" },
--     R = { "<cmd>Telescope registers<cr>", "Registers" },
--     k = { "<cmd>Telescope keymaps<cr>", "Keymaps" },
--     C = { "<cmd>Telescope commands<cr>", "Commands" },
--   },
--   -- t = {
--   --   name = "Terminal",
--   --   n = { "<cmd>lua _NODE_TOGGLE()<cr>", "Node" },
--   --   u = { "<cmd>lua _NCDU_TOGGLE()<cr>", "NCDU" },
--   --   t = { "<cmd>lua _HTOP_TOGGLE()<cr>", "Htop" },
--   --   p = { "<cmd>lua _PYTHON_TOGGLE()<cr>", "Python" },
--   --   f = { "<cmd>ToggleTerm direction=float<cr>", "Float" },
--   --   h = { "<cmd>ToggleTerm size=10 direction=horizontal<cr>", "Horizontal" },
--   --   v = { "<cmd>ToggleTerm size=80 direction=vertical<cr>", "Vertical" },
--   -- },
-- }

-- Use which-key to add extra bindings with the leader-key prefix
-- lvim.builtin.which_key.mappings["P"] = { "<cmd>Telescope projects<CR>", "Projects" }
-- lvim.builtin.which_key.mappings["t"] = {
--   name = "+Trouble",
--   r = { "<cmd>Trouble lsp_references<cr>", "References" },
--   f = { "<cmd>Trouble lsp_definitions<cr>", "Definitions" },
--   d = { "<cmd>Trouble lsp_document_diagnostics<cr>", "Diagnostics" },
--   q = { "<cmd>Trouble quickfix<cr>", "QuickFix" },
--   l = { "<cmd>Trouble loclist<cr>", "LocationList" },
--   w = { "<cmd>Trouble lsp_workspace_diagnostics<cr>", "Diagnostics" },
-- }

-- TODO: User Config for predefined plugins
-- After changing plugin config exit and reopen LunarVim, Run :PackerInstall :PackerCompile
lvim.builtin.dashboard.active = true
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.show_icons.git = 0

-- if you don't want all the parsers change this to a table of the ones you want
lvim.builtin.treesitter.ensure_installed = {
  "bash",
  "c",
  "javascript",
  "json",
  "lua",
  "python",
  "typescript",
  "css",
  "rust",
  "java",
  "yaml",
}

-- lvim.builtin.treesitter.ignore_install = { "haskell" }
-- lvim.builtin.treesitter.highlight.enabled = true

-- generic LSP settings

-- ---@usage disable automatic installation of servers
lvim.lsp.automatic_servers_installation = true

-- ---@usage Select which servers should be configured manually. Requires `:LvimCacheRest` to take effect.
-- See the full default list `:lua print(vim.inspect(lvim.lsp.override))`
-- vim.list_extend(lvim.lsp.override, { "pyright" })

-- ---@usage setup a server -- see: https://www.lunarvim.org/languages/#overriding-the-default-configuration
-- local opts = {} -- check the lspconfig documentation for a list of all possible options
-- require("lvim.lsp.manager").setup("pylsp", opts)

-- you can set a custom on_attach function that will be used for all the language servers
-- See <https://github.com/neovim/nvim-lspconfig#keybindings-and-completion>
-- lvim.lsp.on_attach_callback = function(client, bufnr)
--   local function buf_set_option(...)
--     vim.api.nvim_buf_set_option(bufnr, ...)
--   end
--   --Enable completion triggered by <c-x><c-o>
--   buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")
-- end
-- you can overwrite the null_ls setup table (useful for setting the root_dir function)
-- lvim.lsp.null_ls.setup = {
--   root_dir = require("lspconfig").util.root_pattern("Makefile", ".git", "node_modules"),
-- }
-- or if you need something more advanced
-- lvim.lsp.null_ls.setup.root_dir = function(fname)
--   if vim.bo.filetype == "javascript" then
--     return require("lspconfig/util").root_pattern("Makefile", ".git", "node_modules")(fname)
--       or require("lspconfig/util").path.dirname(fname)
--   elseif vim.bo.filetype == "php" then
--     return require("lspconfig/util").root_pattern("Makefile", ".git", "composer.json")(fname) or vim.fn.getcwd()
--   else
--     return require("lspconfig/util").root_pattern("Makefile", ".git")(fname) or require("lspconfig/util").path.dirname(fname)
--   end
-- end

-- -- set a formatter, this will override the language server formatting capabilities (if it exists)
-- local formatters = require "lvim.lsp.null-ls.formatters"
-- formatters.setup {
--   { exe = "black", filetypes = { "python" } },
--   { exe = "isort", filetypes = { "python" } },
--   {
--     exe = "prettier",
--     ---@usage arguments to pass to the formatter
--     -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
--     args = { "--print-with", "100" },
--     ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
--     filetypes = { "typescript", "typescriptreact" },
--   },
-- }

-- -- set additional linters
-- local linters = require "lvim.lsp.null-ls.linters"
-- linters.setup {
--   { exe = "flake8", filetypes = { "python" } },
--   {
--     exe = "shellcheck",
--     ---@usage arguments to pass to the formatter
--     -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
--     args = { "--severity", "warning" },
--   },
--   {
--     exe = "codespell",
--     ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
--     filetypes = { "javascript", "python" },
--   },
-- }

-- Additional Plugins
-- lvim.plugins = {
--     {"folke/tokyonight.nvim"},
--     {
--       "folke/trouble.nvim",
--       cmd = "TroubleToggle",
--     },
-- }
lvim.plugins = {
  {"dhruvasagar/vim-table-mode"},
  {"Pocco81/AutoSave.nvim"},
  {"ActivityWatch/aw-watcher-vim"},
  {
    'HallerPatrick/py_lsp.nvim'
  },
  {"iamcco/markdown-preview.nvim"}
}
local autosave = require("autosave")
autosave.setup(
    {
        enabled = true,
        execution_message = "AutoSave: saved at " .. vim.fn.strftime("%H:%M:%S"),
        events = {"InsertLeave", "TextChanged"},
        conditions = {
            exists = true,
            filename_is_not = {},
            filetype_is_not = {},
            modifiable = true
        },
        write_all_buffers = false,
        on_off_commands = true,
        clean_command_line_interval = 0,
        debounce_delay = 135
    }
)

-- Autocommands (https://neovim.io/doc/user/autocmd.html)
-- lvim.autocommands.custom_groups = {
--   { "BufWinEnter", "*.lua", "setlocal ts=8 sw=8" },
-- }
